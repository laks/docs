
---
title: ROCK Pi 4
---

Note: A lot of the information here was taken from [Collabora's RockPi4
repository][2].

ROCK Pi 4 is a Rockchip RK3399 based SBC (Single Board Computer) by Radxa.

There are three variants: A, B and C. With the main difference being the
presence of WiFi and Bluetooth on B and C, and the presence of DP and HDMI mini
on C. More information on https://wiki.radxa.com/Rockpi4/getting_started.

Specification:
* Rockchip Rk3399 SoC
* 2x ARM Cortex-A72 @ 1.8GHz, 4x ARM Cortex-A53 @ 1.4GHz
* 1/2/4GB memory
* Mali T860MP4 GPU
* MIPI CSI 2 lanes connector
* eMMC module connector
* uSD slot (up to 128GB)
* 4x USB
* 1x HDMI
* Ethernet port
* 40-pin IO header including UART, SPI, I2C and 5V DC power in
* USB PD over USB Type-C
* Size: 85mm x 54mm

This board is available from [Radxa distributors](https://wiki.radxa.com/Buy).

### Power control

The board can be powered on with 5V via pins 2 or 4 in the GPIO header (as shown
in the [GPIO pinout][1]), and GND on pin 6 for example. The pins are colored
accordingly on the latest board revision:

![Power through GPIO](/img/rockpi4-power-gpio.jpg)

Alternatively, the board can be powered via a USB PD Type-C connector:
* 9V/2A, 12V/2A, 15V/2A, 20V/2A

### Low-level boot control

Serial connection on the board can be done through pins on the GPIO. The board's
TX is on pin 8 and RX on pin 10 as per [the pinout][1]. Serial specification:
* 3.3V TTL
* 1500000 8n1

![Serial through GPIO](/img/rockpi4-serial-gpio.jpg)

#### Network connectivity

There's a standard Gigabit Ethernet port on the board that can be used.

### Bootloader

For the bootloader setup, a ready-to-use image is available at [Collabora's Rock
Pi 4 repository][2] as an artifact from the CI. In addition to u-boot (and
idbloader and trust img), it also contains a kernel and rootfs, but after
flashing and dropping to the u-boot prompt, tftp and nfs can be configured to
retrieve the kernel and rootfs, ignoring the ones flashed.

Load addresses:
* Kernel: 0x280000
* DTB: 0x8300000
* Ramdisk: 0xa200000

The main bootloader is stored in eMMC.

### Health checks

Mainline kernel and defconfig don't have working NFS. For that, [this custom
config](https://people.collabora.com/~nfraprado/config-rockpi4-v5_12) can be
used with mainline v5.12 kernel.

### Lab notes and trouble shooting

None.

[1]: https://wiki.radxa.com/Rockpi4/hardware/gpio
[2]: https://gitlab.collabora.com/rockpi/rockpi4
