---
title: Running Chrome OS in LAVA
weight: 3
---

Running Chrome OS images and dedicated Chrome OS tests come with very specific
requirements.  These tests typically can't be run with any other user-space
than Chrome OS and have to be deployed via the [Tast
framework](https://chromium.googlesource.com/chromiumos/platform/tast/).

## Flashing a Chrome OS image on a Chromebook

The first step is to flash a Chrome OS image onto a Chromebook, as they can't
be booted over NFS.  This can be done within a Debian Buster NFS root with no
special tools installed.  It can be entirely automated in a LAVA job.  Once the
device has booted, the Chrome OS image needs to be downloaded (public location
yet to be determined).  Then here's a sample script to flash the image from
within an NFS root job, assuming the image is called
`octopus-chromiumos-test-image.bin`:

```sh
cd /root
mkdir chromeos
dev=$(losetup -P -f --show octopus-chromiumos-test-image.bin)
mount "$dev"p3 chromeos -o ro
cd chromeos
mount udev -t devtmpfs dev
mount proc -t proc proc
mount sysfs -t sysfs sys
mount -t tmpfs tmpfs tmp
mount --bind /root/ mnt/empty
mount
ls -l mnt/empty/octopus-chromiumos-test-image.bin
ls -l dev/mmc*
echo "Starting to flash..."
chroot . \
  /usr/sbin/chromeos-install \
  --dst /dev/mmcblk0 \
  --payload_image /mnt/empty/octopus-chromiumos-test-image.bin \
  --yes
```

It relies on the `chromeos-install` script which is provided inside the image
itself.  This is done by accessing the main partition (read-only) in the image
via losetup and chroot.  The image file is made available "within itself" by
bind-mounting it in a known empty directory `/mnt/empty`.

## Booting with Chrome OS

Some particular kernel configurations need to be enabled in order for all the
Chrome OS services to fully boot.

_To be continued_

## Running tests with Tast

Once the device has booted Chrome OS, it's possible to start using Tast over
SSH from a separate host (e.g. a LAVA dispatcher).  Tast can be run within
Docker on the host, and this is the approach take with LAVA jobs too.

_To be continued_
